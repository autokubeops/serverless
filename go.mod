module gitlab.com/autokubeops/serverless

go 1.19

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/stretchr/testify v1.8.0
	gopkg.in/yaml.v3 v3.0.1 // indirect
)

require (
	github.com/golang/protobuf v1.5.2 // indirect
	golang.org/x/sys v0.8.0 // indirect
	google.golang.org/protobuf v1.28.0 // indirect
)

require (
	golang.org/x/net v0.10.0
	golang.org/x/text v0.9.0 // indirect
	google.golang.org/grpc v1.46.0
)

require (
	github.com/felixge/httpsnoop v1.0.3
	github.com/gorilla/handlers v1.5.1
	google.golang.org/genproto v0.0.0-20220426171045-31bebdecfb46 // indirect
)

require (
	github.com/KimMachineGun/automemlimit v0.2.2
	github.com/djcass44/go-probe-lib v0.1.2
	github.com/go-logr/logr v1.2.3
	github.com/go-logr/stdr v1.2.2
	go.uber.org/automaxprocs v1.5.2-0.20220426165107-d835ace014b3
)

require (
	github.com/cilium/ebpf v0.4.0 // indirect
	github.com/containerd/cgroups v1.0.4 // indirect
	github.com/coreos/go-systemd/v22 v22.3.2 // indirect
	github.com/docker/go-units v0.4.0 // indirect
	github.com/godbus/dbus/v5 v5.0.4 // indirect
	github.com/gogo/protobuf v1.3.2 // indirect
	github.com/opencontainers/runtime-spec v1.0.2 // indirect
	github.com/sirupsen/logrus v1.8.1 // indirect
	gopkg.in/check.v1 v1.0.0-20190902080502-41f04d3bba15 // indirect
)
