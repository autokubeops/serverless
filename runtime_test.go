package serverless_test

import (
	"context"
	"fmt"
	"github.com/djcass44/go-probe-lib/pkg/probe"
	"github.com/go-logr/logr"
	"github.com/go-logr/logr/testr"
	"github.com/go-logr/stdr"
	"github.com/stretchr/testify/assert"
	"gitlab.com/autokubeops/serverless"
	"math/rand"
	"net/http"
	"testing"
	"time"
)

func TestBuilder_WithProbes(t *testing.T) {
	ctx := logr.NewContext(context.TODO(), testr.NewWithOptions(t, testr.Options{Verbosity: 10}))
	port := rand.Intn(30000) + 2000

	probes := probe.NewHandler(0)

	go func() {
		serverless.NewBuilder(http.NotFoundHandler()).
			WithLogger(logr.FromContextOrDiscard(ctx)).
			WithPort(port).
			WithProbes(probes).
			Run()
	}()

	time.Sleep(time.Millisecond * 100)

	// check that the server is running
	resp, err := http.Get(fmt.Sprintf("http://localhost:%d/", port))
	assert.NoError(t, err)
	assert.EqualValues(t, http.StatusNotFound, resp.StatusCode)

	time.Sleep(time.Millisecond * 100)

	// request shutdown
	assert.NoError(t, probes.Shutdown(ctx))

	time.Sleep(time.Second)

	// check that the server is NOT running
	_, err = http.Get(fmt.Sprintf("http://localhost:%d/", port))
	assert.Error(t, err)
}

func TestRunTLS(t *testing.T) {
	log := testr.NewWithOptions(t, testr.Options{Verbosity: 10})

	go func() {
		serverless.NewBuilder(http.NotFoundHandler()).
			WithLogger(log).
			WithTLS("./testdata/tls.crt", "./testdata/tls.test.key", nil).
			Run()
	}()

	time.Sleep(time.Second * 1)
}

func TestRun(t *testing.T) {
	log := testr.NewWithOptions(t, testr.Options{Verbosity: 10})
	var cases = []struct {
		name     string
		builder  *serverless.Builder
		expected int
	}{
		{
			"handler is served correctly",
			serverless.NewBuilder(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
				_, _ = w.Write([]byte("OK"))
			})).WithHandlers(false).WithLogger(log),
			http.StatusOK,
		},
		{
			"custom log framework is accepted",
			serverless.NewBuilder(http.NotFoundHandler()).WithLogger(stdr.New(nil)),
			http.StatusNotFound,
		},
		{
			"panics are caught when extrahandlers are enabled",
			serverless.NewBuilder(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
				panic("something went wrong")
			})).WithLogger(log),
			http.StatusInternalServerError,
		},
	}
	for _, tt := range cases {
		t.Run(tt.name, func(t *testing.T) {
			// start the server
			port := rand.Intn(30000) + 2000 //nolint:gosec
			go func() {
				tt.builder.
					WithPort(port).
					Run()
			}()
			// wait for the server to start
			time.Sleep(time.Second)
			// run check(s)
			resp, err := http.Get(fmt.Sprintf("http://localhost:%d/", port))
			assert.NoError(t, err)
			assert.NoError(t, resp.Body.Close())
			assert.EqualValues(t, tt.expected, resp.StatusCode)
		})
	}
}
